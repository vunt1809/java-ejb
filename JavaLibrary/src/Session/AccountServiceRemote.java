/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.Customer;
import javax.ejb.Remote;

/**
 *
 * @author admin
 */
@Remote
public interface AccountServiceRemote {
    public Customer createAccount(String firstName, String lastName);
}
